package com.example.johnconstantine.algorithmsproject.ble_package;




import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;


public class BLEListeningService extends Service {

    // <<<<BLE Code!>>>>>
    private BluetoothManager bluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning = false;
    private final static int REQUEST_ENABLE_BT = 1;

    private static final String BEACON1 = "000b";
    private static final String BEACON2 = "000d";

    private String nameOfFile = null;
    private String innerDirectory = null;

    private Handler handler;
    private long startingTime;



    // <<<<BLE Code!>>>>>

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    public BLEListeningService() {
    }

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
            long endTime = System.currentTimeMillis() + 5 * 1000;
            while (System.currentTimeMillis() < endTime) {
                synchronized (this) {
                    try {
                        wait(endTime - System.currentTimeMillis());
                    } catch (Exception e) {
                    }
                }
            }
            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
//			stopSelf(msg.arg1);
        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service. Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block. We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        // HandlerThread thread = new HandlerThread("ServiceStartArguments",
        // Process.THREAD_PRIORITY_BACKGROUND);
        // thread.start();
        //
        // // Get the HandlerThread's Looper and use it for our Handler
        // mServiceLooper = thread.getLooper();
        // mServiceHandler = new ServiceHandler(mServiceLooper);

        // Code from Zach
        // setup the BTLE service

		if (bluetoothManager == null) {
			bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			// Toast.makeText(fragment.getActivity(),
			// "blueToothManager is null", Toast.LENGTH_LONG).show();
			// return; // return causes a crash in the emulator
		}

		if (mBluetoothAdapter == null) {
			mBluetoothAdapter = bluetoothManager.getAdapter();
			// Toast.makeText(fragment.getActivity(),
			// "mBluetoothAdapter is null", Toast.LENGTH_LONG).show();
			// return; // return causes a crash in the emulator
		}






        handler = new Handler();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the
        // job
        // Message msg = mServiceHandler.obtainMessage();
        // msg.arg1 = startId;
        // mServiceHandler.sendMessage(msg);

        // If we get killed, after returning from here, restart
        Bundle extras = intent.getExtras();
        if(extras != null) {
            nameOfFile = extras.getString("fileName");
            innerDirectory = extras.getString("innerDir");
        }


        startingTime = System.currentTimeMillis();
        bleScan();

        return START_NOT_STICKY;
    }

    // <<<<<<<<<<<<<BLE CODE>>>>>>>>>>>>>

    /**
     * This method scans for Bluetooth 4.0 (Low Energy) beacons. It adds any
     * beacon it finds to the beacons list and sets the table's title and fills
     * the table with the new data.
     */
	private void bleScan() {

		// start out with a clean list
		// beaconResults.clear();

		// Scan for available BLE beacons
		if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
			Context test = getBaseContext();
			Intent dialogIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			getApplication().startActivity(dialogIntent);


		}
        mScanning = true;
		scanLeDevice(true);
	}

	/**
	 * This method either starts or stops BLE scanning. We create a handler
	 * which will run after a set amount of time and stops our scanning. This
	 * assures that we will scan BLE long enough to get results but not so long
	 * as it will drain the battery and cause other issues.
	 *
	 * @param enable
	 *            determines if we should be enabling or disabling BLE scanning
     */
	private void scanLeDevice(final boolean enable) {


		if (enable) {
			if (!mBluetoothAdapter.startLeScan(mLeScanCallBack)) {
				// Toast.makeText(fragment.getActivity(),
				// "BLE scan failed to start", Toast.LENGTH_LONG).show();
			}

		} else {

			mBluetoothAdapter.stopLeScan(mLeScanCallBack);
		}
	}

	/**
	 * This method is the callback for the BLE scanning. When we are scanning
	 * and a beacon is found, this callback will be invoked. We can't access the
	 * BLE device on the main thread so we grab all its data on the UI thread.
	 * This is how we populate the beacon array for those results to be shown.
	 *
	 * We also grab some of the data from the byte[] that is passed back. We can
	 * get the UUID, major, minor, and measured 1m RSSI values from this array.
     */
	private BluetoothAdapter.LeScanCallback mLeScanCallBack = new BluetoothAdapter.LeScanCallback() {
		@Override
		public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {

			byte[] uuid = Arrays.copyOfRange(scanRecord, 9, 25); // byte 10 to
			// 25
			// Get the hex string
			String uuidString = bytesToHex(uuid);

			byte[] major = Arrays.copyOfRange(scanRecord, 25, 27);
			String majorString = bytesToHex(major);

			byte[] minor = Arrays.copyOfRange(scanRecord, 27, 29);
			final String minorString = bytesToHex(minor);
            final int myRSSI = rssi;


			final BluetoothDevice bleDevice = device;
			final int deviceRSSI = rssi;
            if(!minorString.equals(BEACON1) && !minorString.equals(BEACON2)){ return; }

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

                    Log.d("Heard Minor" , minorString);
                    //toastThis("Heard minor: " + minorString);
                    //Construct the line to put into the file:
                    //minor value, rssi, timeSinceStart \n
                    appendMessageToFile(minorString + "," + myRSSI + "," +
                            (System.currentTimeMillis() - startingTime) +"\n");

					scanLeDevice(false);
                    if(mScanning){ scanLeDevice(true);}
				}
			});
		}
	};

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }




    public String bytesToHex(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (byte b: bytes) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }


    private void appendMessageToFile(String message){

        if(nameOfFile == null){return;}

        try {
            File root;
            if(innerDirectory == null) {
                root = new File(Environment.getExternalStorageDirectory(),
                        "AlgDumpFolder");
            } else {
                root = new File(Environment.getExternalStorageDirectory(),
                        "AlgDumpFolder/" + innerDirectory);
            }

            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, nameOfFile + ".txt");
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append(message);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            Toast.makeText(this, "Error, error Will Robinson. Unable to write to file.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            // importError = e.getMessage();
            // iError();
        }

    }


    private void toastThis(String message){

        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

    }
    // <<<<<<<<<<<<<BLE CODE>>>>>>>>>>>>>

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {

        mScanning = false;
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
        scanLeDevice(false);

    }
}