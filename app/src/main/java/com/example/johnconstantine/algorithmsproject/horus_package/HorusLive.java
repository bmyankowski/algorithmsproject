package com.example.johnconstantine.algorithmsproject.horus_package;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.johnconstantine.algorithmsproject.R;
import com.example.johnconstantine.algorithmsproject.ble_package.BLEListeningService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import auxiliary.Beacon;
import auxiliary.BeaconTwoFer;

public class HorusLive extends ActionBarActivity {

    private static final String SIGNATURE = "_rs";
    private  ArrayList<File> legitFiles;
    private Beacon beacon1 = new Beacon("000b");
    private Beacon beacon2 = new Beacon("000d");
    private String testFileName;
    private static final int RECORD_TIME = 10000;
    private static final int TICK_TIME = 250;
    private int nForHashMake;
    private String myRecordingSession;

    private boolean testing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horus_live);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_horus_live, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to catch the button click for loading a heat map.
     * Method will search the proper directory to find all recorded sessions.
     * Then present them to the user, allowing them to pick which session to load.
     * @param view don't ask questions, you shouldn't be calling this.
     */
    public void loadHeatmapClicked(View view){
        //So, search the directory for other directories!
        lookForRecordedSessions();

    }


    private void lookForRecordedSessions(){
        //This should get us a list of the files in that folder.
        File folder = new File(Environment.getExternalStorageDirectory(), "AlgDumpFolder");
        //If the folder doesn't exist, tell the user no recorded sessions exist.
        if(!folder.exists()){
            noRecordedSessions();
            return;
        }

        //Get a list of all the files in the folder.
        File[] listOfFiles = folder.listFiles();
        if(listOfFiles.length == 0){
            //Again, no recorded sessions.
            noRecordedSessions();
            return;
        }

        //We should have some recorded sessions to work with, let's make sure we only display the ones
        //that are directories.
        //Make a list of the ones that contain the signature "_rs"
        legitFiles  = new ArrayList<File>();

        for(int i = 0; i < listOfFiles.length; i++){
            if(listOfFiles[i].getPath().contains(SIGNATURE)){
                legitFiles.add(listOfFiles[i]);
            }
        }
        if(legitFiles.isEmpty()){
            noRecordedSessions();
            return;
        }
        //W/ a good list, we can no present to the user, the different sessions.

        //String[] split = legitFiles.get(0).getPath().split("/");
        //split[5] contains the recorded session name.
        //for(int i = 0; i < split.length; i++){ Toast.makeText(this, i + ": " + split[i], Toast.LENGTH_LONG).show();}

        //Get the recorded session name and remove the signature.
        final String[] recordedSessions = new String[legitFiles.size()];
        for(int i = 0; i < legitFiles.size(); i++){
            String temp = legitFiles.get(i).getPath();
            String[] split = temp.split("/");
            String temp2 = split[5].replaceAll(SIGNATURE, "");
            //Toast.makeText(this, "Session name: " + temp2, Toast.LENGTH_LONG).show();
            recordedSessions[i] = temp2;
        }

        //Create the builder.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose a previously recorded session...");
        builder.setItems(recordedSessions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int index) {
                //we get the index, now let the method know which point it needs to worry about.
                for(int i = 0; i < 10; i++) {
                    loadHeatMap(legitFiles.get(index).getPath().split("/")[5]);
                }
            }
        });
        builder.show();
    }


    private void loadHeatMap(String filePath){

        /*BEGIN TIME ANALYSIS*/
        long startTime, endTime;
        long startHeapSize, endHeapSize;
        nForHashMake = 0;
        startHeapSize = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        startTime = System.nanoTime();

        //Toast.makeText(this, "You chose: " + filePath, Toast.LENGTH_LONG).show();
        File folder = new File(Environment.getExternalStorageDirectory(), "AlgDumpFolder/" + filePath );
        File[] listOfFiles = folder.listFiles();
        //We have file path, so let's read in each line.
        for(int i = 0; i < listOfFiles.length; i++) {
            //Need to retrieve the point I'm working with.
            String pointString = listOfFiles[i].getPath().split("/")[6].replace("point", "").replace(".txt", "");
            int point = Integer.valueOf(pointString);
            //Create to list for this point, one for one beacon, one for another.
            ArrayList<Integer> beacon1RSSI = new ArrayList<Integer>(); // 000b
            ArrayList<Integer> beacon2RSSI = new ArrayList<Integer>(); // 000d
            try {
                BufferedReader reader = new BufferedReader(new FileReader(listOfFiles[i].getPath()));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    //Each line will look like this:
                    // minor value, rssi, time since listener was turned on (ms) \n
                    //So, need to compile all this data together. We don't really need time right now.

                    //Since we are expecting two particular minor values to start, I will just create
                    //the beacons beforehand for them. Process will work like this:
                    String[] split = line.split(",");
                    //split[0] contains the minor value.
                    //split[1] contains the rssi value.
                    //split[2] contains the time.

                    nForHashMake++;
                    //Add to beacon 1
                    if(split[0].equals(beacon1.getMinorValue())){
                        beacon1RSSI.add(Integer.valueOf(split[1]));
                    //Add to beacon 2
                    } else {
                        beacon2RSSI.add(Integer.valueOf(split[1]));
                    }

                }
                //Finishing the file off, now I pass each list to their respective beacon.
                beacon1.addPoint(point, beacon1RSSI);
                if(!testing) { beacon2.addPoint(point, beacon2RSSI); }
                //Rinse and repeat till all points read in.



            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        endTime = System.nanoTime();
        endHeapSize = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        /*FINISH TIME ANALYSIS*/

        //Create data string: filePath,time,heapSize,n
        String info = filePath + "," + (endTime - startTime) + "," + (endHeapSize - startHeapSize) + "," + nForHashMake + ",\n";
        recordSpaceAndTime("makeHash", info);

    }


    /**
     * The way this method works is as follows:
     * Give me two rssi s, I'll get a list from each beacon the probability of that signal
     * showing up at each point.
     * NOW, for each point, I'll muiltiply the probability from each beacon w/ each other
     * if either is zero, then the probability the total probability will be zero.
     *
     * Once I get the probabilities, I'll have to normalize them, so I can accurately compare.
     *
     * Then, sort by most probable ...and return the most probable point.
     * @param beacon1RSSI
     * @param beacon2RSSI
     * @return
     */
    private int getMostProbablePoint(int beacon1RSSI, int beacon2RSSI){
        ArrayList<BeaconTwoFer> beacon1Twofers = beacon1.getMostProbablePoint(beacon1RSSI);
        ArrayList<BeaconTwoFer> beacon2Twofers = beacon1.getMostProbablePoint(beacon2RSSI);


        //Now multiply the two.
        ArrayList<BeaconTwoFer> overallTwofers = new ArrayList<BeaconTwoFer>();
        double normalizer = 0;
        for(int i = 0; i < beacon1Twofers.size(); i++){
            for(int j = 0; j < beacon2Twofers.size(); j++){
                //If they aren't the same size, there's a bigger problem.
                if(beacon1Twofers.get(i).getPointName() == beacon2Twofers.get(j).getPointName()){
                    BeaconTwoFer temp = new BeaconTwoFer();
                    temp.setPointName(beacon1Twofers.get(i).getPointName());
                    double newProb =  beacon1Twofers.get(i).getProbability() * beacon2Twofers.get(j).getProbability();
                    temp.setProbability(newProb);
                    overallTwofers.add(temp);
                    normalizer += newProb;
                    break;
                }
            }


        }
        if(normalizer == 0){ normalizer = 1;}
        //Go through new list real quick and normalize probabilities.
        for(int i = 0; i < overallTwofers.size(); i++){
            overallTwofers.get(i).setProbability(overallTwofers.get(i).getProbability() / normalizer );
        }

        Collections.sort(overallTwofers, new ProbCompare());

        //Need to write what I results found in a file.
        String dataForFile = "\nGiven the following RSSIs beacon1: " + beacon1RSSI + ", beacon2: " + beacon2RSSI + ", I list the points with their probabilities:\n";
        for(int i = 0; i < overallTwofers.size(); i++){
            dataForFile += "Point: " + overallTwofers.get(i).getPointName() + "\tProb: " + overallTwofers.get(i).getProbability() + "\n";
        }
        writePointProbsToFile(dataForFile);

        return overallTwofers.get(0).getPointName();
    }

    class ProbCompare implements Comparator<BeaconTwoFer> {
        public int compare(BeaconTwoFer a, BeaconTwoFer b) {
            return a.getProbability() > b.getProbability() ? -1 : a.getProbability() == b.getProbability() ? 0 : 1;
        }
    }


    public void runTest(View view){
        //Need to run test.

        //Operations:
        //  1) I need to let the user choose a test batch to run.
        //  2) Give them all the different tests taken.
        //  3) Load up the test that they have chosen.
        //  4) Group data properly
        //          a) First by minor value
        //          b) Second by into chunks, so.. I expect the user to walk through the group, and touch 3 points.
        //              given this assumption (and force to start), I'm going to chunk the data in 33 percentiles.
        //              Each 33 percent, I'll average the data together.
        //              That average will represent the reading from that point. From there, I'll try and determine the 3
        //              points that the user walked through. If the graph created is reasonable, then I'll consider it a success.


        //Start off be kicking of service, and naming the file/directory it'll be in.
        //Get name of test.
        getNameOfTestingSession();
    }


    private void getNameOfTestingSession(){
        //Get all the files within the test folder.
        //Look inside ../AlgDumpFolder/TestBatches.
        //This should get us a list of the files in that folder.
        File folder = new File(Environment.getExternalStorageDirectory(), "AlgDumpFolder/TestBatches");
        //If the folder doesn't exist, tell the user no recorded sessions exist.
        if(!folder.exists()){
            noTestBatches();
            return;
        }

        //Get a list of all the files in the folder.
        File[] listOfFiles = folder.listFiles();
        if(listOfFiles.length == 0){
            //Again, no recorded sessions.
            noTestBatches();
            return;
        }

        //We should have some test batches to work with, let's make sure we only display the ones
        //that are text files.
        //Make a list of the ones that contain the signature ".txt"
        legitFiles  = new ArrayList<File>();

        for(int i = 0; i < listOfFiles.length; i++){
            if(listOfFiles[i].getPath().contains(".txt")){
                legitFiles.add(listOfFiles[i]);
            }
        }
        //If none of them are text files...oh bother.
        if(legitFiles.isEmpty()){
            noTestBatches();
            return;
        }
        //W/ a good list, we can no present to the user, the different sessions.

       // String[] split = legitFiles.get(0).getPath().split("/");
        //split[6] contains the recorded session name.
       // for(int i = 0; i < split.length; i++){ Toast.makeText(this, i + ": " + split[i], Toast.LENGTH_LONG).show();}
        //ToDo: Return after actually making test batches.

        //Get the recorded session name and remove the signature.
        final String[] recordedSessions = new String[legitFiles.size()];
        for(int i = 0; i < legitFiles.size(); i++){
            String temp = legitFiles.get(i).getPath();
            String[] split = temp.split("/");
            String temp2 = split[6].replaceAll(SIGNATURE, "");
            //Toast.makeText(this, "Session name: " + temp2, Toast.LENGTH_LONG).show();
            recordedSessions[i] = temp2;
        }

        for(int i = 0; i < legitFiles.size(); i++){
            for(int j = 0; j < 10; j++){
                testFileName = legitFiles.get(i).getPath().split("/")[6];
                if(testFileName.contains("2k")){
                    beginAnalysis();
                }
            }
        }

/*        //Create the builder.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose a previously recorded session...");
        builder.setItems(recordedSessions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int index) {
                //we get the index, now let the method know which point it needs to worry about.
                testFileName = legitFiles.get(index).getPath().split("/")[6];
                beginAnalysis();

            }
        });
        builder.show();*/


    }

    private void noTestBatches(){
        Toast.makeText(this, "You have not created any test batches. Please exit to the main menu and do so.", Toast.LENGTH_LONG).show();
    }



/*    private void beginRecording(){
        //Since the test file doesn't already exist (and now it's been created, we can kick off the hoodinger.
        final Intent i = new Intent(this, BLEListeningService.class);
        i.putExtra("fileName" , testFileName);
        i.putExtra("innerDir", "HorusTest");
        startService(i);
        new CountDownTimer(RECORD_TIME, TICK_TIME) {//(total time, tick)

            public void onTick(long millisUntilFinished) {
                //Don't do anything.
            }

            public void onFinish() {
                //Kill service.
                stopService(i);
                beginAnalysis();
            }
        }.start();


    }*/

    private void beginAnalysis() {
        //Toast.makeText(this, "Test file name: " + testFileName, Toast.LENGTH_LONG).show();
        //Open file, get the contents, then organize.
        File file = new File(Environment.getExternalStorageDirectory(), "AlgDumpFolder/TestBatches/" + testFileName);
        long startTime, endTime;
        long startHeapSize, endHeapSize;
        int n = 0;
        startHeapSize = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

        ArrayList<Integer> beacon1RSSI = new ArrayList<Integer>(); // 000b
        ArrayList<Integer> beacon2RSSI = new ArrayList<Integer>(); // 000d
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = reader.readLine()) != null) {
                //Each line will look like this:
                // minor value, rssi, time since listener was turned on (ms) \n
                //So, need to compile all this data together. We don't really need time right now.

                //Since we are expecting two particular minor values to start, I will just create
                //the beacons beforehand for them. Process will work like this:
                String[] split = line.split(",");
                //split[0] contains the minor value.
                //split[1] contains the rssi value.
                //split[2] contains the time.


                n++;
                //Add to beacon 1
                if (split[0].equals(beacon1.getMinorValue())) {
                    beacon1RSSI.add(Integer.valueOf(split[1]));
                    //Add to beacon 2
                } else {
                    beacon2RSSI.add(Integer.valueOf(split[1]));
                }

            }


            /*BEGIN TIME ANALYSIS*/
            startTime = System.nanoTime();

            //Should have two lists now, I need to make 5 points for each.
            //Because they may not be the same sizes, we'll just run two loops WHOO!

            //To find the percents of each...
            double counterPercentage = .33;
            Integer[] beacon1Points = new Integer[3];
            int currentAverage = 0;
            int readingsCounter = 1;
            int numOfAverages = 0;
            for(int i = 0; i < beacon1RSSI.size(); i++){
                double currentPercent = (double)(i + 1) / beacon1RSSI.size();
                if(currentPercent < counterPercentage){
                    //Just add to our running average.
                    currentAverage += beacon1RSSI.get(i);
                    //Increment the counter of the number of points we got.
                    readingsCounter++;
                //If we are finally over that percentage...
                } else {
                    //Increase to next block.
                    counterPercentage += .33;
                    //finish calcing average (w/o using new point).
                    beacon1Points[numOfAverages] = currentAverage / readingsCounter;
                    //Reset to one.
                    readingsCounter = 1;
                    //Just set current average to this point.
                    currentAverage = beacon1RSSI.get(i);
                    //Increase the index for our averages.
                    numOfAverages++;
                }

            }

            counterPercentage = .33;
            Integer[] beacon2Points = new Integer[3];
            currentAverage = 0;
            readingsCounter = 1;
            numOfAverages = 0;
            for(int i = 0; i < beacon2RSSI.size(); i++){
                double currentPercent = (double)(i + 1) / beacon2RSSI.size();
                if(currentPercent < counterPercentage){
                    //Just add to our running average.
                    currentAverage += beacon2RSSI.get(i);
                    //Increment the counter of the number of points we got.
                    readingsCounter++;
                    //If we are finally over that percentage...
                } else {
                    //Increase to next block.
                    counterPercentage += .33;
                    //finish calcing average (w/o using new point).
                    beacon2Points[numOfAverages] = currentAverage / readingsCounter;
                    //Reset to one.
                    readingsCounter = 1;
                    //Just set current average to this point.
                    currentAverage = beacon2RSSI.get(i);
                    //Increase the index for our averages.
                    numOfAverages++;
                }

            }



            //Should have points now, I'll compose answers.

            //Now, let's find the most probable points.
            Integer[] points = new Integer[3];

            for(int i = 0; i < 3; i++){
                try {
                    points[i] = getMostProbablePoint(beacon1Points[i], beacon2Points[i]);
                } catch (Exception e){
                    String message = "Not enough points were heard.\nFAILED";
                    writePointProbsToFile(message);
                    return;
                }
            }

/*            if(runGraphCheck(points)){
                Toast.makeText(this, "You entered the building!", Toast.LENGTH_LONG).show();
                String message = "PASSED.";
                writePointProbsToFile(message);
            } else {
                Toast.makeText(this, "There appears to be mistake, sir, you didn't " +
                        "enter the building.", Toast.LENGTH_LONG).show();
                writePointProbsToFile("FAILED");
            }*/


            endHeapSize = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
            endTime = System.nanoTime();
            /*FINISH TIME ANALYSIS*/

            //Write the output info in following format: testFile,time,heapSize,sizeOfN\n
            String info = testFileName + "," + (endTime - startTime) + "," + (endHeapSize - startHeapSize) + "," + n + ",\n";
            outputTimeSpace(info);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean runGraphCheck(Integer[] points) {
        //So, check to make sure that each of these points follows the correct path.

        //We should see 1->2->3
        //or 3->2->1
        if(points[0] == 1 && points[1] == 2 && points[2] == 3){return true;}
        else if (points[0] == 3 && points[1] == 2 && points[2] == 1){return true;}
        //else return false
        return false;



    }


    private void outputTimeSpace(String info) {
        String fileName = "TimeAndSpaceHorus";
        try {
            File root;
            root = new File(Environment.getExternalStorageDirectory(),
                    "AlgDumpFolder/TimeSpace");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, fileName + ".txt");
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append(info);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            Toast.makeText(this, "Error, error Will Robinson. Unable to write to file.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            // importError = e.getMessage();
            // iError();
        }
    }


    private void writePointProbsToFile(String message){

        String fileName = "PointProbs:" + testFileName;
        try {
            File root;
            root = new File(Environment.getExternalStorageDirectory(),
                    "AlgDumpFolder/HorusTestResults");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, fileName + ".txt");
            FileWriter writer = new FileWriter(gpxfile,true);
            writer.append(message);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            Toast.makeText(this, "Error, error Will Robinson. Unable to write to file.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            // importError = e.getMessage();
            // iError();
        }

    }



    private void recordSpaceAndTime(String fileName, String data){

        try {
            File root;
            root = new File(Environment.getExternalStorageDirectory(),
                    "AlgDumpFolder/TimeSpace");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, fileName + ".txt");
            FileWriter writer = new FileWriter(gpxfile,true);
            writer.append(data);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            Toast.makeText(this, "Error, error Will Robinson. Unable to write to file.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            // importError = e.getMessage();
            // iError();
        }


    }


    private void noRecordedSessions(){
        Toast.makeText(this, "No previously recorded sessions.", Toast.LENGTH_LONG).show();

    }
}
