package com.example.johnconstantine.algorithmsproject.niave_approach;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.johnconstantine.algorithmsproject.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import auxiliary.Beacon;

public class NiaveApproach extends ActionBarActivity {

    private String testFileName;
    private ArrayList<File> legitFiles;
    private static final String SIGNATURE = "_rs";
    private static final double INDEX_THRESHOLD = 3;
    private ArrayList<Integer> forcedLineBeacon1 = new ArrayList<Integer>();
    private ArrayList<Integer> forcedLineBeacon2 = new ArrayList<Integer>();
    private Beacon beacon1 = new Beacon("000b");
    private Beacon beacon2 = new Beacon("000d");
    //Yay. Information.
    //SOOOOOOOOOOO.
    //Let's suppose n = the number of data points I have recorded.
    //Suppose N = the number of points I'm going to average over.
    //Meaning, ( (x1 - xN) / N ) = my first avg point.
    //         ( (x2 - x(N + 1) ) / N ) = my second avg point.
    //         ... etc.
    //So, using a little bit of wit and after a glass of tasty rootbeer, I have some equations.
    //Suppose A = the total number of avg points I can calculate.
    // A = n - N + 1 : so long as N <= n (trust me I did a nifty induction proof...I have interesting weekends, I promise :P)
    // Now, A/n = some fraction of the original amount.
    // And I want to constrain N to produce a particular amount (50, 60, 75 percent of the original amount of data, etc.)
    //SO A/n = t, where t is some fractional amount, 1/2 -> 50% (forget % was a key haha), 3/4 -> 75% etc.
    //We can solve for N in terms of n, I'll skip the calculations here, and just show you the solution.
    // To produce a particular t, N = n (1 - t) + 1.
    //So, w/ all these neato information, I'm going to define the variables here.

    //Down in my analysis method, I'll set n and N.
    private static final double THRESHOLD = (double) 9 / 10;
    private int n1;
    private int N1;
    private int n2;
    private int N2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_niave_approach);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_niave_approach, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void niaveTestOnClick(View view) {
        //Begin test. Gather data, then pull from the file it was written too.
        getNameOfTestingSession();

    }

    private void getNameOfTestingSession() {
        //Get all the files within the test folder.
        //Look inside ../AlgDumpFolder/TestBatches.
        //This should get us a list of the files in that folder.
        File folder = new File(Environment.getExternalStorageDirectory(), "AlgDumpFolder/TestBatches");
        //If the folder doesn't exist, tell the user no recorded sessions exist.
        if (!folder.exists()) {
            noTestBatches();
            return;
        }

        //Get a list of all the files in the folder.
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles.length == 0) {
            //Again, no recorded sessions.
            noTestBatches();
            return;
        }

        //We should have some test batches to work with, let's make sure we only display the ones
        //that are text files.
        //Make a list of the ones that contain the signature ".txt"
        legitFiles = new ArrayList<File>();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].getPath().contains(".txt")) {
                legitFiles.add(listOfFiles[i]);
            }
        }
        //If none of them are text files...oh bother.
        if (legitFiles.isEmpty()) {
            noTestBatches();
            return;
        }
        //W/ a good list, we can no present to the user, the different sessions.

        // String[] split = legitFiles.get(0).getPath().split("/");
        //split[6] contains the recorded session name.
        // for(int i = 0; i < split.length; i++){ Toast.makeText(this, i + ": " + split[i], Toast.LENGTH_LONG).show();}
        //ToDo: Return after actually making test batches.

        //Get the recorded session name and remove the signature.
        final String[] recordedSessions = new String[legitFiles.size()];
        for (int i = 0; i < legitFiles.size(); i++) {
            String temp = legitFiles.get(i).getPath();
            String[] split = temp.split("/");
            String temp2 = split[6].replaceAll(SIGNATURE, "");
            //Toast.makeText(this, "Session name: " + temp2, Toast.LENGTH_LONG).show();
            recordedSessions[i] = temp2;
        }


        for (int i = 0; i < legitFiles.size(); i++) {
            for (int j = 0; j < 10; j++) {
                testFileName = legitFiles.get(i).getPath().split("/")[6];
                beginAnalysis();
            }
        }


/*
        //Create the builder.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose a previously recorded session...");
        builder.setItems(recordedSessions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int index) {
                //we get the index, now let the method know which point it needs to worry about.
                testFileName = legitFiles.get(index).getPath().split("/")[6];
                beginAnalysis();

            }
        });
        builder.show();
*/


    }

    private void beginAnalysis() {
        File file = new File(Environment.getExternalStorageDirectory(), "AlgDumpFolder/TestBatches/" + testFileName);

        long startTime, endTime;
        long startHeapSize, endHeapSize;
        int n = 0;
        startHeapSize = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        ArrayList<Integer> beacon1RSSI = new ArrayList<Integer>(); // 000b
        ArrayList<Integer> beacon2RSSI = new ArrayList<Integer>(); // 000d
        ArrayList<Integer> time1 = new ArrayList<Integer>(); // 000b
        ArrayList<Integer> time2 = new ArrayList<Integer>(); // 000d
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = reader.readLine()) != null) {
                //Each line will look like this:
                // minor value, rssi, time since listener was turned on (ms) \n
                //So, need to compile all this data together. We don't really need time right now.

                //Since we are expecting two particular minor values to start, I will just create
                //the beacons beforehand for them. Process will work like this:
                String[] split = line.split(",");
                //split[0] contains the minor value.
                //split[1] contains the rssi value.
                //split[2] contains the time.
                n++;

                //Add to beacon 1
                if (split[0].equals(beacon1.getMinorValue())) {
                    beacon1RSSI.add(Integer.valueOf(split[1]));
                    time1.add(Integer.valueOf(split[2]));
                    //Add to beacon 2
                } else {
                    beacon2RSSI.add(Integer.valueOf(split[1]));
                    time2.add(Integer.valueOf(split[2]));
                }

            }
            if (beacon1RSSI.isEmpty() || beacon2RSSI.isEmpty()) {
                writeToFile("We didn't hear both beacons, test FAILED.");
                return;
            }

            /*BEGIN TIME/SPACE ANALYSIS*/
            startTime = System.nanoTime();


            //Now ...we need to apply the moving average algorithm.
            n1 = beacon1RSSI.size();
            n2 = beacon2RSSI.size();
            N1 = (int) (n1 * (1 - THRESHOLD) + 1);
            N2 = (int) (n2 * (1 - THRESHOLD) + 1);

            //Using these fancy values, I'll create my "fake" lines.
            int numbaOfPointsToGenerate1 = n1 - N1 + 1;
            int numbaOfPointsToGenerate2 = n2 - N2 + 1;

            int avg1 = 0;
            int avg2 = 0;

            //Generate average for first N1 and N2 points.
            for (int i = 0; i < N1; i++) {
                avg1 += beacon1RSSI.get(i);
            }
            forcedLineBeacon1.add(avg1 / N1);
            for (int i = 0; i < N2; i++) {
                avg2 += beacon2RSSI.get(i);
            }
            forcedLineBeacon2.add(avg2 / N2);

            //Now, cycle through the num of points we have left.
            for (int i = (N1 + 1); i < beacon1RSSI.size(); i++) {
                avg1 = avg1 - beacon1RSSI.get(i - N1) + beacon1RSSI.get(i);
                forcedLineBeacon1.add(avg1 / N1);
            }

            for (int i = (N2 + 1); i < beacon2RSSI.size(); i++) {
                avg2 = avg2 - beacon2RSSI.get(i - N2) + beacon2RSSI.get(i);
                forcedLineBeacon2.add(avg2 / N2);
            }
            //Assuming this worked, we'll apply the following niave approach.
            //So, we want to detect a rise and fall. For our purposes, we want to detect an exit = slow rise, fast fall.
            //To detect this, I'll simply be looking for the maximum in each list. (If I've done the previous part right,
            //then we should have a pretty simple line contained in each resepective arraylist.) Anyhoo, a "good" line
            //will have the max rather late. This means 1 <= j < MAX < i <= n. j > (n - i)   <== That means we walked through the door.
            //W/ this set up, we should be able to detect false-positives and what not.
            //Run loop looking for maxes!

            int max1 = -100;
            int maxIndex1 = 0;
            int numOfMaxes = 0;

            int max2 = -100;
            int maxIndex2 = -1;


            //Because there could be multiple maxes, I need to add up all the indecies of the maxes found,
            //and average them together.
            for (int i = 0; i < forcedLineBeacon1.size(); i++) {
                if (forcedLineBeacon1.get(i) > max1) {
                    max1 = forcedLineBeacon1.get(i);
                    maxIndex1 = i;
                    numOfMaxes = 1;
                } else if (forcedLineBeacon1.get(i) == max1) {
                    maxIndex1 += i;
                    numOfMaxes++;
                }
            }
            //Now, I need to divide the maxIndecies by the number of them that I found.
            maxIndex1 /= numOfMaxes;
            //Same process for two.
            numOfMaxes = 0;
            for (int i = 0; i < forcedLineBeacon2.size(); i++) {
                if (forcedLineBeacon2.get(i) > max2) {
                    max2 = forcedLineBeacon2.get(i);
                    maxIndex2 = i;
                    numOfMaxes = 1;
                } else if (forcedLineBeacon2.get(i) == max2) {
                    maxIndex2 += i;
                    numOfMaxes++;
                }
            }
            maxIndex2 /= numOfMaxes;
            //Now look at their placement in the list.
            boolean check1 = false, check2 = false;// check3 = false;//, check4 = false;
            if (((double) (maxIndex1 + 1) / forcedLineBeacon1.size()) > ((double) 1 / 2)) {
                check1 = true;
            }
            if (((double) (maxIndex2 + 1) / forcedLineBeacon2.size()) > ((double) 1 / 2)) {
                check2 = true;
            }
            //if( Math.abs(maxIndex1 - maxIndex2) > INDEX_THRESHOLD){check3 = true;}
            int size1 = forcedLineBeacon1.size();
            int size2 = forcedLineBeacon2.size();
            int difference = Math.abs(size1 - size2);
            int sum = size1 + size2;
            //if( (  ((double)difference) / (sum) ) < .25  ){check4 = true;}

/*            //crazy new idea. Look for the max, and make sure it's after 15000 seconds.
            int max1 = -200, max2 = -2500, maxIndex1 = -1, maxIndex2 = -1;
            for (int i = 0; i < beacon1RSSI.size(); i++) {
                if (beacon1RSSI.get(i) > max1) {
                    max1 = beacon1RSSI.get(i);
                    maxIndex1 = i;
                }
            }
            for (int i = 0; i < beacon2RSSI.size(); i++) {
                if (beacon2RSSI.get(i) > max2) {
                    max2 = beacon2RSSI.get(i);
                    maxIndex2 = i;
                }
            }

            if (time1.get(maxIndex1) > 15000) {
                check1 = true;
            }
            if (time2.get(maxIndex2) > 15000) {
                check2 = true;
            }*/






            /*FINISH TIME/SPACE ANALYSIS*/
            endHeapSize = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
            endTime = System.nanoTime();

            //Toast.makeText(this,  "startHeapSize: " + startHeapSize + "\tendHeapSize: " + endHeapSize, Toast.LENGTH_SHORT).show();

            //Write the output info in following format: testFile,time,heapSize,sizeOfN\n
            String info = testFileName + "," + (endTime - startTime) + "," + (endHeapSize - startHeapSize) + "," + n + ",\n";
            outputTimeSpace(info);

            String dataForFile = "";
/*            if(check1 && check2 ){
                Toast.makeText(this, "Yay, you walked through the door!", Toast.LENGTH_LONG).show();
                dataForFile += "PASSED\n";
            } else {
                dataForFile += "FAILED\n";
                Toast.makeText(this, "You did not walk through the door, :/ look at your output files.", Toast.LENGTH_LONG).show();

            }*/

/*
            //Need to write data to files, so I can examine what went right and wrong.

            dataForFile += "This analysis was done on the test batch: " + testFileName + "\n";
            dataForFile += "Test results for beacon1.\nMax: " + max1 + "  indexOfMax: " + maxIndex1 + "\n";
            dataForFile += "Here is the arrayList after running the moving average.\n";
            for(int i = 0; i < forcedLineBeacon1.size(); i++){
                dataForFile += forcedLineBeacon1.get(i) + ",\n";
            }
            dataForFile += "\nTest results for beacon2.\nMax: " + max2 + " indexOfMax: " + maxIndex2  + "\n";
            dataForFile += "Here is the arrayList after running the moving average.\n";
            for(int i = 0; i < forcedLineBeacon2.size(); i++){
                dataForFile += forcedLineBeacon2.get(i) + ",\n";
            }
            //Write this to a file.
            writeToFile(dataForFile);*/


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void writeToFile(String message) {

        String fileName = "TestResults:" + testFileName;
        try {
            File root;
            root = new File(Environment.getExternalStorageDirectory(),
                    "AlgDumpFolder/NaiveTestResults");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, fileName + ".txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(message);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            Toast.makeText(this, "Error, error Will Robinson. Unable to write to file.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            // importError = e.getMessage();
            // iError();
        }

    }


    private void outputTimeSpace(String info) {
        String fileName = "TimeAndSpaceNaiveMoving";
        try {
            File root;
            root = new File(Environment.getExternalStorageDirectory(),
                    "AlgDumpFolder/TimeSpace");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, fileName + ".txt");
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append(info);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            Toast.makeText(this, "Error, error Will Robinson. Unable to write to file.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            // importError = e.getMessage();
            // iError();
        }
    }

    private void noTestBatches() {
        Toast.makeText(this, "You have not created any test batches. Please exit to the main menu and do so.", Toast.LENGTH_LONG).show();
    }
}
