package com.example.johnconstantine.algorithmsproject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.johnconstantine.algorithmsproject.ble_package.BLEListeningService;
import com.example.johnconstantine.algorithmsproject.horus_package.HorusMainActivity;
import com.example.johnconstantine.algorithmsproject.niave_approach.NiaveApproach;

import java.io.File;


public class MainActivity extends ActionBarActivity {

    private Intent bleListenerIntent;
    private String testBatchName;
    private boolean canIStartTheTest = false;
    private EditText messageBox;
    private Context activityContext = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, "Hello World!", Toast.LENGTH_LONG).show();
        messageBox = (EditText) findViewById(R.id.messageEditText);
        messageBox.setKeyListener(null);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBLEClicked(View view) {
        // Is the toggle on?
        boolean on = ((ToggleButton) view).isChecked();

        if (on) {
            // Enable Listener
            //First, get the user to name this test batch.
            getFileName("Please enter the name of this test batch: ");

        } else {
            // Disable Listener
            if (bleListenerIntent == null) {
                messageBox.setText("You have disabled the test mode. You need to re-enable it to run a test.");

                canIStartTheTest = false;
            } else {


            }
        }


    }

    public void onNiaveClick(View view){
        Intent i = new Intent(this, NiaveApproach.class);
        startActivity(i);
    }

    public void onBeginTestClicked(View view) {
        //This will start a test.
        view.setVisibility(View.INVISIBLE);
        messageBox.setText("The test will begin in 3 seconds. At the sound of a tone, you may begin the test." +
                " You will have 30 seconds to complete the test. Begin out of range of the BLE beacons and end " +
                "out of range of the BLE beacons.");
        new CountDownTimer(3000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                messageBox.setText("Starting in: " + millisUntilFinished / 1000);

            }

            @Override
            public void onFinish() {
                messageBox.setText("Test has begun.");
                makeSomeNoise();
            }
        }.start();

        bleListenerIntent = new Intent(this, BLEListeningService.class);
        bleListenerIntent.putExtra("fileName" , testBatchName);
        bleListenerIntent.putExtra("innerDir", "TestBatches");
        //ToDo: Set up timer to tell the user test is about to begin.
        startService(bleListenerIntent);

        new CountDownTimer(30000, 10000) {

            @Override
            public void onTick(long millisUntilFinished) {
                Toast.makeText(activityContext, (millisUntilFinished / 1000) + " seconds remaining.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                finishTest();
            }
        }.start();


    }

    private void finishTest(){
        messageBox.setText("You have finished your test. The results are in the appropriate folder.");
        stopService(bleListenerIntent);
        bleListenerIntent = null;
        canIStartTheTest = false;
        ( (ToggleButton) findViewById(R.id.bleListener)).setChecked(false);

    }

    private static ToneGenerator toneG = null;

    private void makeSomeNoise() {
        AudioManager am;
        Vibrator vibrator;
        am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        long pattern[] = {0, 200, 100, 300, 400};

        if (toneG != null) {
            toneG.release();
        }
        // we want to play the tone at the current volume level
        int volume_level = am.getStreamVolume(AudioManager.STREAM_RING);
        int max_level = am.getStreamMaxVolume(AudioManager.STREAM_RING);

        double volume = ((double) volume_level / (double) max_level) * 100;

        toneG = new ToneGenerator(AudioManager.STREAM_ALARM, (int) volume);
        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);

        // start vibration with repeated count, use -1 if you don't want to
        // repeat the vibration
        if (vibrator != null) vibrator.vibrate(pattern, -1);
    }

    public void onHorusClicked(View view) {

        //Launch Horus Activity.
        Intent i = new Intent(this, HorusMainActivity.class);
        startActivity(i);

    }

    private void getFileName(String message) {
        //First, get a the name of this recording session.
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Recording Session:");
        alert.setMessage(message);

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String toLower = input.getText().toString();
                testBatchName = toLower.toLowerCase() + "_rs";
                checkFileName();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                testBatchName = null;
            }
        });

        alert.show();

    }

    /**
     * Need to make sure the filename pasted in doesn't exist.
     */
    private void checkFileName() {
        File root = new File(Environment.getExternalStorageDirectory(),
                "AlgDumpFolder/TestBatches/" + testBatchName + ".txt");
        if (root.exists()) {
            //If it exists, let the user know they can't use that name.
            getFileName("That Test Batch already exists," +
                    "choose a different name.\nEnter the name of this recording session: ");
        } else {
            //That dir doesn't exists, so create new dir.
//            root.mkdirs();
            //Allow user to begin their test run.
            messageBox.setVisibility(View.VISIBLE);
            messageBox.setText("You are now enabled to begin the test. Please press the start button to recieve further instructions");

            canIStartTheTest = true;
            findViewById(R.id.beginTestButton).setVisibility(View.VISIBLE);
        }

    }
}
