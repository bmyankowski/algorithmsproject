package com.example.johnconstantine.algorithmsproject.horus_package;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.johnconstantine.algorithmsproject.R;
import com.example.johnconstantine.algorithmsproject.ble_package.BLEListeningService;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class HorusHeatMap extends ActionBarActivity {

    private String recordingSession;
    private ArrayList<TwoFer> points = new ArrayList<TwoFer>();
    private int pointsLeftToRecord = -1;
    private static final int RECORD_TIME = 30000;
    private static final int TICK_TIME = 250;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horus_heat_map);

        //Create the array of points.
        for(int i = 1; i < 4; i++){
            //Create points 1 - 12.
            TwoFer temp = new TwoFer();
            temp.pointName = i;
            points.add(temp);
        }

        //Set progress bar.
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_horus_heat_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onRecordDataClicked(View view){
        progressBar.setProgress(0);

        //Clicked the data record button.
        if(recordingSession == null) {
            getDirName("Enter the name of this recording session: ");
        } else {
            //Since we have the dir name, ask what point we need to record.
            askWhichPoint();
        }



    }


    private void getDirName(String message){
        //First, get a the name of this recording session.
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Recording Session:");
        alert.setMessage(message);

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String toLower = input.getText().toString();
                recordingSession = toLower.toLowerCase() + "_rs";
                checkDirName();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                recordingSession = null;
            }
        });

        alert.show();

    }

    /**
     * Need to make sure the filename pasted in doesn't exist.
     */
    private void checkDirName(){
        File root = new File(Environment.getExternalStorageDirectory(),
                "AlgDumpFolder/" + recordingSession);
        if(root.exists()){
            //If it exists, let the user know they can't use that name.
            getDirName("That recording session already exists," +
                    "choose a different name.\nEnter the name of this recording session: ");
        } else {
            //That dir doesn't exists, so create new dir.
            root.mkdirs();
            //Now we'll ask the user what point they are recording? Giving them a list of up to 12 points.
            askWhichPoint();

        }

    }

    /**
     * Asks the user which point they want to record.
     */
    private void askWhichPoint(){
        //First make a list
        //First, get all the points that have not been recorded.
        Iterator<TwoFer> twoFerIterator = points.iterator();

        //If we don't know how many points left to record (i.e. hasn't been set, check);
        if(pointsLeftToRecord == -1) {
            pointsLeftToRecord = 0;
            while (twoFerIterator.hasNext()){
                if(!twoFerIterator.next().recorded){
                    pointsLeftToRecord++;
                }
            }

        }
        //Now, since we have a non-negative value set to string array.
        //if zero, return telling the user they are done.
        if(pointsLeftToRecord == 0){
            Toast.makeText(this, "You've recorded all the points you can for this session.", Toast.LENGTH_LONG).show();
            return;
        }
        final String[] listOfOptions = new String[pointsLeftToRecord];
        twoFerIterator = points.iterator();
        int index = 0;
        while(twoFerIterator.hasNext()){
            TwoFer temp = twoFerIterator.next();
            //If we havn't recorded, than put it in the list as an option.
            if(!temp.recorded){
                listOfOptions[index] = String.valueOf(temp.pointName);
                index++;
            }
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose a point to record...");
        builder.setItems(listOfOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int index) {
               //we get the index, now let the method know which point it needs to worry about.
                recordPoint(listOfOptions[index]);


            }
        });
        builder.show();


    }

    /**
     * Method recieves a point to record, quickly go through list find it, and record data for it.s
     * @param pointToRecord String (1-3)
     */
    private void recordPoint(String pointToRecord){

        int workingIndex = -1;

        //Find point in list.
        for(int i = 0;  i < points.size(); i++){
            if(pointToRecord == ( String.valueOf(points.get(i).pointName) ) ){
                workingIndex = i;
                break;
            }
        }
        if(workingIndex == -1){
            Toast.makeText(this, "Error finding point.", Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(this, "Working with point: " + points.get(workingIndex).pointName, Toast.LENGTH_LONG).show();
        final int theIndex = workingIndex;
        /* Do work */
        //So, here I'm going to set up a countdown timer. The timer starts the service that records the BLE signals.
        //Each tick will update the progress bar, just so the recorder (me) will know how much longer they have.
        //When the countdown timer finishes, it will stop the service.

        //The pasted in file name will be point<workingIndex>. Simple enough and generic.
        //However, we will need to pass in the directory as well. BRB.
        final Intent i =  new Intent(this, BLEListeningService.class);
        i.putExtra("innerDir", recordingSession);
        i.putExtra("fileName", "point" + points.get(workingIndex).pointName);
        startService(i);
        new CountDownTimer(RECORD_TIME, TICK_TIME) {//(total time, tick)

            public void onTick(long millisUntilFinished) {
                //Update progress bar.
                if(progressBar.getProgress() < 100){
                    double percentDone = ((double)RECORD_TIME - millisUntilFinished) / RECORD_TIME;
                    double update = percentDone * 100;
                    progressBar.setProgress(  (int) update  );
                }

            }

            public void onFinish() {
                progressBar.setProgress(100);
                //Change boolean to reflect change.
                points.get(theIndex).recorded = true;
                pointsLeftToRecord--;
                //Kill service.
                stopService(i);
            }
        }.start();



    }





    private class TwoFer{

        boolean recorded = false;
        int pointName;



    }
}
