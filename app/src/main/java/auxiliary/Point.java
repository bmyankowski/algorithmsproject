package auxiliary;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * A point is rather simple. It contains all the different rssi's heard at said point,
 * in addition to the probability of that RSSI. WHOOT WHOOT MOFO.
 * <p/>
 * Created by John Constantine on 3/22/2015.
 */
public class Point {

    ArrayList<TwoFer> twoFers = new ArrayList<TwoFer>();
    double[] rssiHashTable;
    private int name;

    /**
     * This constructor takes in a list of RSSI values.
     * Using the values it will create a twofer for each RSSI value.
     *
     * @param RSSIvalues ArrayList<int>
     */
    public Point(int numba, ArrayList<Integer> RSSIvalues) {
        name = numba;

        //Method for creating each point.
        createHashTable(RSSIvalues);
        //For legacy reasons, I'll leave in this old method of storing info, which still works if un-commented.
/*        Iterator<Integer> integerIterator = RSSIvalues.iterator();
        int totalReadings = 0;
        while(integerIterator.hasNext()){
            int temp = integerIterator.next();
            //Need to search the list of twofers for this guy, seeing if he is already in there.
            boolean recorded = false;
            for(int i = 0; i < twoFers.size(); i++){
                if(temp == twoFers.get(i).RSSI){
                    twoFers.get(i).probability++;
                    recorded = true;
                    break;
                }
            }
            if(!recorded){
                TwoFer temp2fer = new TwoFer();
                temp2fer.RSSI = temp;
                temp2fer.probability = 1;
                twoFers.add(temp2fer);
            }
            totalReadings++;
        }

        //Okay, read in all points, now I'm going to....divide each probability by the total numba of readings.
        for(int i = 0; i < twoFers.size(); i++){
            twoFers.get(i).probability /= totalReadings;
        }*/

        //Now, everything should be settled out.
    }


    public int getName() {
        return name;
    }

    /**
     * This method will take in an RSSI value, and return the probability of
     * being at this particular point given the RSSI value.
     *
     * @param rssi int
     * @return double <- probability of being at this point. Returns 0 if RSSI didn't exist.
     */
    public double probabilityGivenRSSI(int rssi) {

        //This was my old method for determining our probability.
/*        for(int i = 0; i < twoFers.size(); i++){
            if(twoFers.get(i).RSSI == rssi){
                return twoFers.get(i).probability;
            }
        }*/

        //New method hashes into the table to find the frequency.
        //First, just hash into and see what we get.
        int index = Math.abs(rssi) % rssiHashTable.length; //Length is what we determined to be our hash function.\
        double probability = rssiHashTable[index];

        //If we hashed to a spot w/ positive probability, then return it. (Constant time woo!)
        if (probability != 0) {
            return probability;
        }

        //If it was zero, I'm not giving up yet!
        //We will peform an alternating search. Search above our intended, search below our intended spot.
        //Until we find a value. However, we need to devalue the probabilty each time we take a step farther away.
        //This wasn't in the Horus Paper, they just assumed that they would get every possible signal or if there isn't a signal
        //recorded at a spot it meant it wasn't possible. That's why they suggested 5 minutes for a record time, because that was
        //determined enough time to get all possible points.
        //Me being nifty, I'm going to assume it is still possible not to hear the signals. SO, let's try my method.
        //Each time we take a step away from the index, we will reduce the probability found by 10%. So, after 10 tries, the probability
        //should be zero. If that's the case, then this signal isn't in the possible range.


        for (int i = 1; i < 10; i++) {
            //We'll add/subtract i to rssi value and re-hash.
            int below = (Math.abs(rssi) - i) % rssiHashTable.length;
            int above = (Math.abs(rssi) + i) % rssiHashTable.length;

            //either doesn't equal zero, return it.
            //Arbitrarily start above, then check below.

            if (rssiHashTable[above] != 0) {
                return (rssiHashTable[above] - (rssiHashTable[above] * (i * .1)));
            }


            if (rssiHashTable[below] != 0) {
                return (rssiHashTable[below] - (rssiHashTable[below] * (i * .1)));
            }


        }

        //If after checking 10 below and above we don't find any non-zero values, return 0.0

        return 0.0;
    }


    /**
     * This method is to replace the current method of storing our RSSI values, it should be speedy quick.
     * We are given an arrayList of rssi values, from the list we will gather the following information:
     * The max and min RSSI values (this gives us the size of our hash table.
     * The frequency of each RSSI value. (when we hash to a slot in the table, if it's not NULL we will increment the counter for it.)
     *
     * @param RSSIvalues ArrayList<Integer>
     */
    private void createHashTable(ArrayList<Integer> RSSIvalues) {
        int max = -200; //Arbitrary value, RSSI values are never this small.
        int min = 100; //Again, arbitrary value, RSSI values are never this small.
        Iterator<Integer> integerIterator = RSSIvalues.iterator();
        while (integerIterator.hasNext()) {
            int temp = integerIterator.next();
            if (temp > max) {
                max = temp;
            }
            if (temp < min) {
                min = temp;
            }
        }
        //So, first value will become both the min and max, but other values will replace (at least one of those) soon after.

        //With those, we now compute the size of the hash table.
        int hashTableSize = max - min + 1; //Add one because we are inclusive of the max and min.
        rssiHashTable = new double[hashTableSize];
        //Now, we hash each value to the table.
        //Java is quite fortuitous, as everything in this table is initialized to a zero.
        //We loop through now, and hash everything to the table based on the size of the table i.e.
        // index = RSSI_value % hashTableSize
        // rssiHashTable[index] ++ <- increase the frequency count, if we never hash to a slot, it equals zero!

        for (int i = 0; i < RSSIvalues.size(); i++) {
            int index = Math.abs(RSSIvalues.get(i) ) % hashTableSize;
            rssiHashTable[index]++;
        }
        //Now, we perform a second loop where we divide by the total number of signals we heard.
        for (int i = 0; i < rssiHashTable.length; i++) {
            rssiHashTable[i] /= RSSIvalues.size();
        }

        //And boom! in linear time we have logged all values.

    }


    private class TwoFer {
        int RSSI;
        double probability;
    }
}
