package auxiliary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * This class represents a Beacon. It must be contructed like so:
 * Specified in a static final field is the number of points it can be heard at.
 *  Each point must be given a set of data taken over 5 minutes. (See method for exact details for passing in)
 *  Once every point has been filled, you may ask the Beacon which point you are at (by giving it an RSSI)
 *
 * Created by John Constantine on 3/22/2015.
 */
public class Beacon {

    private static final int NUMBA_OF_POINTS = 12;
    private String minorValue;


    public Beacon(String minorValue){
        this.minorValue = minorValue;
    }


    public String getMinorValue(){return minorValue;}

    ArrayList<Point> points = new ArrayList<Point>();


    public boolean addPoint(int point, ArrayList<Integer> rssiValues){

        //First, check to see if point already exists.
        for(int i = 0; i < points.size(); i++){
            if(points.get(i).getName() == point){
                return false;
            }
        }

        //Making it through means point didn't exist.

        //Now create new point.
        points.add(new Point(point, rssiValues));

        return true;
    }

    public ArrayList<BeaconTwoFer> getMostProbablePoint(int rssi){

        ArrayList<BeaconTwoFer> twoFers = new ArrayList<BeaconTwoFer>();
        for(int i = 0; i < points.size(); i++){
            BeaconTwoFer temp =  new BeaconTwoFer();
            temp.pointName = points.get(i).getName();
            temp.probability = points.get(i).probabilityGivenRSSI(rssi);
            twoFers.add(temp);
        }

        //If by some weird happening twoFers is empty...then return -1;
        if(twoFers.isEmpty()){ return null; }

        //Find the most probable.
        //Sort in descending order.
        return twoFers;

        //First element should have the "most probable."



    }






}
