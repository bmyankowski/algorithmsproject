package auxiliary;

/**
 * Created by John Constantine on 3/22/2015.
 */
public class BeaconTwoFer {


    int pointName;
    double probability;

    public void setPointName(int inc) {
        pointName = inc;
    }

    public int getPointName() {
        return pointName;
    }

    public void setProbability(double inc) {
        probability = inc;
    }

    public double getProbability() {
        return probability;
    }

}
